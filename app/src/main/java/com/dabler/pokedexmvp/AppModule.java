package com.dabler.pokedexmvp;

import android.app.Application;

import com.dabler.pokedexmvp.pokemonList.PokemonListInterfaces;
import com.dabler.pokedexmvp.pokemonList.PokemonsListPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class AppModule {

    Application application;

    public AppModule(Application application){
        this.application = application;
    }

    @Provides
    @Singleton
    Application provideApplication(){
        return application;
    }

}
