package com.dabler.pokedexmvp;

import com.dabler.pokedexmvp.network.NetModule;
import com.dabler.pokedexmvp.pokemonList.PokemonsListFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, NetModule.class, PresentersModule.class})
public interface PresentersComponent {
    void inject(PokemonsListFragment activity);
}
