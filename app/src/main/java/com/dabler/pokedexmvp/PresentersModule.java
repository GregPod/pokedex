package com.dabler.pokedexmvp;

import com.dabler.pokedexmvp.pokemonDetails.PokemonDetailsPresenter;
import com.dabler.pokedexmvp.pokemonList.PokemonsListPresenter;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class PresentersModule {

    @Provides
    PokemonsListPresenter providePokemonsListPresenter(Retrofit retrofit){
        return new PokemonsListPresenter(retrofit);
    }

    @Provides
    PokemonDetailsPresenter providePokemonDetailsPresenter(Retrofit retrofit){
        return new PokemonDetailsPresenter(retrofit);
    }
}
