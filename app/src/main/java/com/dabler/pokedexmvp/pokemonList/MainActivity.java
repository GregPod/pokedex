package com.dabler.pokedexmvp.pokemonList;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.dabler.pokedexmvp.R;

public class MainActivity extends AppCompatActivity {

    PokemonsListFragment pokemonsListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pokemonsListFragment = PokemonsListFragment.newInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, pokemonsListFragment)
                .commit();
    }
}
