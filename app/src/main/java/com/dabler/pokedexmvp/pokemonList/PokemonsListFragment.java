package com.dabler.pokedexmvp.pokemonList;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dabler.pokedexmvp.App;
import com.dabler.pokedexmvp.R;
import com.dabler.pokedexmvp.model.PokemonEntry;
import com.dabler.pokedexmvp.pokemonDetails.PokemonDetailsActivity;
import com.hannesdorfmann.mosby3.mvp.MvpFragment;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PokemonsListFragment extends MvpFragment<PokemonListInterfaces.View, PokemonsListPresenter>
        implements PokemonListInterfaces.View, PokemonListInterfaces.RecyclerItemClickListener {

    @BindView(R.id.pokemons_recycler_view)
    RecyclerView pokemonsRecyclerView;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.error_text_view)
    TextView errorTextView;

    @Inject
    PokemonsListPresenter presenter;

    PokemonsAdapter pokemonsAdapter;

    public PokemonsListFragment() {
        // Required empty public constructor
    }

    @Override
    public PokemonsListPresenter createPresenter() {
        return presenter;
    }

    public static PokemonsListFragment newInstance() {
        return new PokemonsListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getActivity().getApplication()).getNetComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pokemons_list, container, false);
        ButterKnife.bind(this, view);

        initializeRecyclerView();
        loadData(false);
        return view;
    }

    @Override
    public void showLoading(boolean pullToRefresh) {
        //no-op
    }

    @Override
    public void showContent() {
        //no-op
    }

    @Override
    public void showError(Throwable e, boolean pullToRefresh) {
        errorTextView.setVisibility(View.VISIBLE);
    }

    @Override
    public void setData(List<PokemonEntry> data) {
        progressBar.setVisibility(View.GONE);
        pokemonsAdapter.addPokemons(data);
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        errorTextView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        presenter.loadData();
    }

    @Override
    public void onItemClickListener(String pokemonUrl) {
        getPresenter().onRecyclerItemClick(pokemonUrl);
    }

    public void openPokemonDetailsActivity(int pokemonId) {
        PokemonDetailsActivity.startActivity(getContext(), pokemonId);
    }

    private void initializeRecyclerView() {
        pokemonsAdapter = new PokemonsAdapter(this);
        pokemonsRecyclerView.setHasFixedSize(true);
        pokemonsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        pokemonsRecyclerView.setAdapter(pokemonsAdapter);
    }
}
