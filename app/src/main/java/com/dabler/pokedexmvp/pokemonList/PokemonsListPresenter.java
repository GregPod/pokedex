package com.dabler.pokedexmvp.pokemonList;

import com.dabler.pokedexmvp.network.PokedexService;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class PokemonsListPresenter extends MvpBasePresenter<PokemonListInterfaces.View> {

    private static final String POKEMON_SPECIE_URL = "http://pokeapi.co/api/v2/pokemon-species/";

    private Retrofit retrofit;

    @Inject
    public PokemonsListPresenter(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    void loadData() {
        retrofit.create(PokedexService.class).getPokedex()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(pokedex -> {
                    if (getView() != null) {
                        getView().setData(pokedex.getPokemonEntries());
                    }
                }, error -> {
                    if (getView() != null) {
                        getView().showError(error, false);
                    }
                });
    }

    void onRecyclerItemClick(String pokemonUrl) {
        if (getView() != null)
            getView().openPokemonDetailsActivity(getPokemonIdFromUrl(pokemonUrl));
    }

    private int getPokemonIdFromUrl(String pokemonSpecieUrl) {
        String trimmedUrl = pokemonSpecieUrl.replace(POKEMON_SPECIE_URL, "").replace("/", "");
        return Integer.parseInt(trimmedUrl);
    }
}
