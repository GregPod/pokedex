package com.dabler.pokedexmvp.pokemonList;

import com.dabler.pokedexmvp.model.PokemonEntry;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

import java.util.List;

public interface PokemonListInterfaces {

    interface View extends MvpLceView<List<PokemonEntry>> {
        void openPokemonDetailsActivity(int pokemonId);
    }

    interface RecyclerItemClickListener {
        void onItemClickListener(String pokemonUrl);
    }
}
