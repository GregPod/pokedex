package com.dabler.pokedexmvp.pokemonList;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dabler.pokedexmvp.R;
import com.dabler.pokedexmvp.model.PokemonEntry;

import java.util.ArrayList;
import java.util.List;

public class PokemonsAdapter extends RecyclerView.Adapter<PokemonsAdapter.ViewHolder> {
    private List<PokemonEntry> pokemons;
    PokemonListInterfaces.RecyclerItemClickListener clickListener;

    public PokemonsAdapter(PokemonListInterfaces.RecyclerItemClickListener clickListener) {
        this.clickListener = clickListener;
        pokemons = new ArrayList<>();
    }

    void addPokemons(List<PokemonEntry> pokemons) {
        this.pokemons = pokemons;
        this.notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pokemon_card_view, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textView.setText(pokemons.get(position).getPokemonSpecies().getName());
    }

    @Override
    public int getItemCount() {
        return pokemons.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textView;

        ViewHolder(View v) {
            super(v);
            textView = v.findViewById(R.id.pokemon_name);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            clickListener.onItemClickListener(pokemons.get(getLayoutPosition()).getPokemonSpecies().getUrl());
        }
    }
}
