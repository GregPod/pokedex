package com.dabler.pokedexmvp;

import android.app.Application;

import com.dabler.pokedexmvp.network.DaggerNetComponent;
import com.dabler.pokedexmvp.network.NetComponent;
import com.dabler.pokedexmvp.network.NetModule;

public class App extends Application {

    private NetComponent netComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        netComponent = DaggerNetComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule(BuildConfig.API_URL))
                .presentersModule(new PresentersModule())
                .build();
    }

    public NetComponent getNetComponent(){
        return netComponent;
    }
}
