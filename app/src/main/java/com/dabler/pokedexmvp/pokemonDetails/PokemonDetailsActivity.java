package com.dabler.pokedexmvp.pokemonDetails;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.dabler.pokedexmvp.R;

public class PokemonDetailsActivity extends AppCompatActivity {

    public static String POKEMON_ID_INTENT_KEY = "pokemonId";
    public static int POKEMON_ID_DEFAULT = -1;

    private int pokemonId;

    public static void startActivity(Context context, int pokemonId) {
        Intent intent = new Intent(context, PokemonDetailsActivity.class);
        intent.putExtra(POKEMON_ID_INTENT_KEY, pokemonId);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon_details);

        initializeFromIntent(getIntent());
        addPokemonDetailsFragment();
    }

    private void initializeFromIntent(Intent intent) {
        pokemonId = intent.getIntExtra(POKEMON_ID_INTENT_KEY, POKEMON_ID_DEFAULT);
    }

    private void addPokemonDetailsFragment() {
        PokemonDetailsFragment fragment = PokemonDetailsFragment.newInstance(pokemonId);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.fragment_container, fragment);
        transaction.commit();
    }
}
