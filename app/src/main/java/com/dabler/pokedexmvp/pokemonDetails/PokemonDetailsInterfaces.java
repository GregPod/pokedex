package com.dabler.pokedexmvp.pokemonDetails;

import com.dabler.pokedexmvp.model.PokemonSpecie;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;


public interface PokemonDetailsInterfaces {

    interface View extends MvpLceView<PokemonSpecie> {
        //no-op
    }
}
