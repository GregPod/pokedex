package com.dabler.pokedexmvp.pokemonDetails;

import com.dabler.pokedexmvp.network.PokedexService;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class PokemonDetailsPresenter extends MvpBasePresenter<PokemonDetailsInterfaces.View> {

    private Retrofit retrofit;

    public PokemonDetailsPresenter(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    void loadData(int pokemonId) {
        retrofit.create(PokedexService.class).getPokemonSpecie(pokemonId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(pokemon ->
                {
                    if (getView() != null)
                        getView().setData(pokemon);
                }, error -> {
                    if (getView() != null) {
                        getView().showError(error, false);
                    }
                });
    }

}
