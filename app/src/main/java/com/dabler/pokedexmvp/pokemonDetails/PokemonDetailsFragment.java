package com.dabler.pokedexmvp.pokemonDetails;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dabler.pokedexmvp.App;
import com.dabler.pokedexmvp.R;
import com.dabler.pokedexmvp.model.PokemonSpecie;
import com.hannesdorfmann.mosby3.mvp.MvpFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.dabler.pokedexmvp.pokemonDetails.PokemonDetailsActivity.POKEMON_ID_INTENT_KEY;

public class PokemonDetailsFragment extends MvpFragment<PokemonDetailsInterfaces.View, PokemonDetailsPresenter>
        implements PokemonDetailsInterfaces.View {

    private final static int ENGLISH_LANGUAGE_ID = 1;

    @BindView(R.id.pokemon_name)
    TextView pokemonName;

    @BindView(R.id.pokemon_id_text_view)
    TextView pokemonIdText;

    @BindView(R.id.pokemon_description)
    TextView pokemonDescription;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.error_text_view)
    TextView errorTextView;

    @Inject
    PokemonDetailsPresenter presenter;

    int pokemonId = -1;

    public PokemonDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public PokemonDetailsPresenter createPresenter() {
        return presenter;
    }

    public static PokemonDetailsFragment newInstance(int pokemonId) {
        PokemonDetailsFragment fragment = new PokemonDetailsFragment();

        Bundle bundle = new Bundle();
        bundle.putInt(POKEMON_ID_INTENT_KEY, pokemonId);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getActivity().getApplication()).getNetComponent().inject(this);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            pokemonId = bundle.getInt(POKEMON_ID_INTENT_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pokemon_details, container, false);
        ButterKnife.bind(this, view);

        loadData(false);
        return view;
    }

    @Override
    public void showLoading(boolean pullToRefresh) {
        //no-op
    }

    @Override
    public void showContent() {
        //no-op
    }

    @Override
    public void showError(Throwable e, boolean pullToRefresh) {
        errorTextView.setVisibility(View.VISIBLE);
    }

    @Override
    public void setData(PokemonSpecie data) {
        progressBar.setVisibility(View.GONE);
        pokemonName.setText(data.getName());
        pokemonIdText.setText(getString(R.string.pokemon_number, data.getId()));
        pokemonDescription.setText(data.getFlavorTexts().get(ENGLISH_LANGUAGE_ID).getFlavorText());
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        errorTextView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        presenter.loadData(pokemonId);
    }

}
