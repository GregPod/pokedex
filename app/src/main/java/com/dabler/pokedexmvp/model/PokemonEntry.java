
package com.dabler.pokedexmvp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PokemonEntry
{

    @SerializedName("pokemon_species")
    @Expose
    private PokemonSpecies pokemonSpecies;

    public PokemonSpecies getPokemonSpecies() {
        return pokemonSpecies;
    }

}
