package com.dabler.pokedexmvp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PokemonSpecie {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("flavor_text_entries")
    @Expose
    private List<FlavorText> flavorTexts;


    public int getId() {
        return id;
    }

    public List<FlavorText> getFlavorTexts() {
        return flavorTexts;
    }

    public String getName() {
        return name;
    }
}
