
package com.dabler.pokedexmvp.model;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pokedex
{
    @SerializedName("pokemon_entries")
    @Expose
    private List<PokemonEntry> pokemonEntries = null;

    public List<PokemonEntry> getPokemonEntries() {
        return pokemonEntries;
    }
}
