package com.dabler.pokedexmvp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FlavorText {

    public String getFlavorText() {
        return flavorText;
    }

    @SerializedName("flavor_text")
    @Expose
    private String flavorText;
}
