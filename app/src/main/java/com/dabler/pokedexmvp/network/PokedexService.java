package com.dabler.pokedexmvp.network;

import com.dabler.pokedexmvp.model.Pokedex;
import com.dabler.pokedexmvp.model.PokemonSpecie;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface PokedexService {

    @GET("api/v2/pokedex/2/")
    Observable<Pokedex> getPokedex();

    @GET("/api/v2/pokemon-species/{id}/")
    Observable<PokemonSpecie> getPokemonSpecie(@Path("id") int id);
}
