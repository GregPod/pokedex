package com.dabler.pokedexmvp.network;

import com.dabler.pokedexmvp.AppModule;
import com.dabler.pokedexmvp.PresentersModule;
import com.dabler.pokedexmvp.pokemonDetails.PokemonDetailsFragment;
import com.dabler.pokedexmvp.pokemonList.MainActivity;
import com.dabler.pokedexmvp.pokemonList.PokemonsListFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, NetModule.class, PresentersModule.class})
public interface NetComponent {
    void inject(PokemonsListFragment activity);
    void inject(PokemonDetailsFragment activity);
}