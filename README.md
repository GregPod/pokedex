# README #

 Simple showcase pokedex app. Enjoy. 
 
### What is this repository for? ###

This repository contains sample Pokedex app. It uses PokeApi to fetch Pokemons data.

### Features ###
* Pokemons list
* Pokemon details

### Screenshots ###
![Pokemons list](https://i.imgur.com/TzxJ3ac.png)
![Pokemon details](https://i.imgur.com/EBYiJag.png)

### How do I get set up? ###
Clone repo, open in Android Studio > 3.0 and open on device/emulator > API 17.

### Used libraries ###
* RxJava2
* Dagger2
* Retrofit
* OkHttp
* Butterknife
